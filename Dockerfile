FROM maven:3.6.3-jdk-11-slim AS MAVEN_BUILD

LABEL maintainer="Igor Sily Developers igorsily2@gmail.com"
COPY pom.xml /build/
COPY src /build/src/
WORKDIR /build/
RUN mvn clean install -Dspring.profiles.active=$SPRING_ACTIVE_PROFILE && mvn package -B -e -Dspring.profiles.active=$SPRING_ACTIVE_PROFILE

FROM openjdk:11-slim
WORKDIR /app

COPY --from=MAVEN_BUILD /build/target/survey-*.jar /app/survey.jar
ENTRYPOINT ["java", "-Dspring.profiles.active=$SPRINT_ACTIVE_PROFILE", "-jar", "survey.jar"]
